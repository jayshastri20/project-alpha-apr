from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


@login_required(redirect_field_name="login")
def project_list(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {
        "projects_list": projects_list,
    }
    return render(request, "projects/list.html", context)


def redirect_project_list(request):
    response = redirect("list_projects")
    return response


@login_required(redirect_field_name="login")
def project_detail(request, id):
    projects_detail = get_object_or_404(Project, id=id)
    context = {
        "projects_detail": projects_detail,
    }
    return render(request, "projects/detail.html", context)


@login_required(redirect_field_name="login")
def project_create(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
