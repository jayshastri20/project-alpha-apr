from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(redirect_field_name="login")
def task_create(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required(redirect_field_name="login")
def task_list(request):
    tasks_list = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_list": tasks_list,
    }
    return render(request, "tasks/list.html", context)
